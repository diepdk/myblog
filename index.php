<?php
	require_once(__DIR__ .'/lib/boostrap.php');
	// echo $_SERVER['HTTP_HOST'];
	// echo "</br>";
	// echo $_SERVER['REQUEST_URI'];

	$admin_pattern = '/^(\/admin)(\/)*((.)*)*/';
	if(preg_match(pattern, $_SERVER['HTTP_URI'])){
		include(getcwd(). '/app/backend/index.php');
	}else{
		$template  = getcwd(). '/app/fontend/view';
		$cache = getcwd() . '/caches';

		// Cau hinh
		$loader = new Twig_Loader_Filesystem($template);
		$twig = new Twig_Environment($loader,array('cache' => $cache , 'auto_reload' => true , 'debug' => true));
		include('router.php');
	}
?>