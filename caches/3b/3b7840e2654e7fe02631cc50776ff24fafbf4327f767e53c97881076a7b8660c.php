<?php

/* index.html */
class __TwigTemplate_79be65d3964b8fd2aca937d857952f976a17678b18006e66f03c56b9750cae1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html", "index.html", 1);
        $this->blocks = array(
            'page_head' => array($this, 'block_page_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_page_head($context, array $blocks = array())
    {
        // line 4
        echo "  <header class=\"masthead\" style=\"background-image: url('assets/img/home-bg.jpg')\">
    <div class=\"overlay\"></div>
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-8 col-md-10 mx-auto\">
          <div class=\"site-heading\">
            <h1>My Blog</h1>
            <span class=\"subheading\">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>
";
    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
        // line 19
        echo "  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-lg-8 col-md-10 mx-auto\">
        <div class=\"post-preview\">
          <a href=\"post.html\">
            <h2 class=\"post-title\">
              Man must explore, and this is exploration at its greatest
            </h2>
            <h3 class=\"post-subtitle\">
              Problems look mighty small from 150 miles up
            </h3>
          </a>
          <p class=\"post-meta\">Posted by
            <a href=\"#\">Start Bootstrap</a>
            on September 24, 2019</p>
        </div>
        <hr>
        <div class=\"post-preview\">
          <a href=\"post.html\">
            <h2 class=\"post-title\">
              I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.
            </h2>
          </a>
          <p class=\"post-meta\">Posted by
            <a href=\"#\">Start Bootstrap</a>
            on September 18, 2019</p>
        </div>
        <hr>
        <div class=\"post-preview\">
          <a href=\"post.html\">
            <h2 class=\"post-title\">
              Science has not yet mastered prophecy
            </h2>
            <h3 class=\"post-subtitle\">
              We predict too much for the next year and yet far too little for the next ten.
            </h3>
          </a>
          <p class=\"post-meta\">Posted by
            <a href=\"#\">Start Bootstrap</a>
            on August 24, 2019</p>
        </div>
        <hr>
        <div class=\"post-preview\">
          <a href=\"post.html\">
            <h2 class=\"post-title\">
              Failure is not an option
            </h2>
            <h3 class=\"post-subtitle\">
              Many say exploration is part of our destiny, but it’s actually our duty to future generations.
            </h3>
          </a>
          <p class=\"post-meta\">Posted by
            <a href=\"#\">Start Bootstrap</a>
            on July 8, 2019</p>
        </div>
        <hr>
        <!-- Pager -->
        <div class=\"clearfix\">
          <a class=\"btn btn-primary float-right\" href=\"#\">Older Posts &rarr;</a>
        </div>
      </div>
    </div>
  </div>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 19,  49 => 18,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'layout.html' %}*/
/* */
/* {% block page_head %}*/
/*   <header class="masthead" style="background-image: url('assets/img/home-bg.jpg')">*/
/*     <div class="overlay"></div>*/
/*     <div class="container">*/
/*       <div class="row">*/
/*         <div class="col-lg-8 col-md-10 mx-auto">*/
/*           <div class="site-heading">*/
/*             <h1>My Blog</h1>*/
/*             <span class="subheading">A Blog Theme by Start Bootstrap</span>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </header>*/
/* {% endblock %}*/
/* {% block content %}*/
/*   <div class="container">*/
/*     <div class="row">*/
/*       <div class="col-lg-8 col-md-10 mx-auto">*/
/*         <div class="post-preview">*/
/*           <a href="post.html">*/
/*             <h2 class="post-title">*/
/*               Man must explore, and this is exploration at its greatest*/
/*             </h2>*/
/*             <h3 class="post-subtitle">*/
/*               Problems look mighty small from 150 miles up*/
/*             </h3>*/
/*           </a>*/
/*           <p class="post-meta">Posted by*/
/*             <a href="#">Start Bootstrap</a>*/
/*             on September 24, 2019</p>*/
/*         </div>*/
/*         <hr>*/
/*         <div class="post-preview">*/
/*           <a href="post.html">*/
/*             <h2 class="post-title">*/
/*               I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.*/
/*             </h2>*/
/*           </a>*/
/*           <p class="post-meta">Posted by*/
/*             <a href="#">Start Bootstrap</a>*/
/*             on September 18, 2019</p>*/
/*         </div>*/
/*         <hr>*/
/*         <div class="post-preview">*/
/*           <a href="post.html">*/
/*             <h2 class="post-title">*/
/*               Science has not yet mastered prophecy*/
/*             </h2>*/
/*             <h3 class="post-subtitle">*/
/*               We predict too much for the next year and yet far too little for the next ten.*/
/*             </h3>*/
/*           </a>*/
/*           <p class="post-meta">Posted by*/
/*             <a href="#">Start Bootstrap</a>*/
/*             on August 24, 2019</p>*/
/*         </div>*/
/*         <hr>*/
/*         <div class="post-preview">*/
/*           <a href="post.html">*/
/*             <h2 class="post-title">*/
/*               Failure is not an option*/
/*             </h2>*/
/*             <h3 class="post-subtitle">*/
/*               Many say exploration is part of our destiny, but it’s actually our duty to future generations.*/
/*             </h3>*/
/*           </a>*/
/*           <p class="post-meta">Posted by*/
/*             <a href="#">Start Bootstrap</a>*/
/*             on July 8, 2019</p>*/
/*         </div>*/
/*         <hr>*/
/*         <!-- Pager -->*/
/*         <div class="clearfix">*/
/*           <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* {% endblock %}*/
