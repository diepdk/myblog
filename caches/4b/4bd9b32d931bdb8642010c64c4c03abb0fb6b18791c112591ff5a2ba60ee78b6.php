<?php

/* about.html */
class __TwigTemplate_727c5436ee42c2761ec38dccc92a3e16e340c194d43464c85854f60a8aea7cdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html", "about.html", 1);
        $this->blocks = array(
            'page_head' => array($this, 'block_page_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_page_head($context, array $blocks = array())
    {
        // line 4
        echo "  <header class=\"masthead\" style=\"background-image: url('../../../assets/img/about-bg.jpg')\">
    <div class=\"overlay\"></div>
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-8 col-md-10 mx-auto\">
          <div class=\"page-heading\">
            <h1>About Me</h1>
            <span class=\"subheading\">This is what I do.</span>
          </div>
        </div>
      </div>
    </div>
  </header>
";
    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
        // line 20
        echo "  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-lg-8 col-md-10 mx-auto\">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe nostrum ullam eveniet pariatur voluptates odit, fuga atque ea nobis sit soluta odio, adipisci quas excepturi maxime quae totam ducimus consectetur?</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius praesentium recusandae illo eaque architecto error, repellendus iusto reprehenderit, doloribus, minus sunt. Numquam at quae voluptatum in officia voluptas voluptatibus, minus!</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut consequuntur magnam, excepturi aliquid ex itaque esse est vero natus quae optio aperiam soluta voluptatibus corporis atque iste neque sit tempora!</p>
      </div>
    </div>
  </div>
";
    }

    public function getTemplateName()
    {
        return "about.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 20,  49 => 19,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'layout.html' %}*/
/* */
/* {% block page_head %}*/
/*   <header class="masthead" style="background-image: url('../../../assets/img/about-bg.jpg')">*/
/*     <div class="overlay"></div>*/
/*     <div class="container">*/
/*       <div class="row">*/
/*         <div class="col-lg-8 col-md-10 mx-auto">*/
/*           <div class="page-heading">*/
/*             <h1>About Me</h1>*/
/*             <span class="subheading">This is what I do.</span>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </header>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*   <div class="container">*/
/*     <div class="row">*/
/*       <div class="col-lg-8 col-md-10 mx-auto">*/
/*         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe nostrum ullam eveniet pariatur voluptates odit, fuga atque ea nobis sit soluta odio, adipisci quas excepturi maxime quae totam ducimus consectetur?</p>*/
/*         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius praesentium recusandae illo eaque architecto error, repellendus iusto reprehenderit, doloribus, minus sunt. Numquam at quae voluptatum in officia voluptas voluptatibus, minus!</p>*/
/*         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut consequuntur magnam, excepturi aliquid ex itaque esse est vero natus quae optio aperiam soluta voluptatibus corporis atque iste neque sit tempora!</p>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* {% endblock %}*/
/* */
/* */
