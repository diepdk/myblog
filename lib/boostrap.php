<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
	require_once(__DIR__ . '/config.php');
	require_once( __DIR__ . '/global.php');

	spl_autoload_register(function($classname){
			include_once(__DIR__ . '/'. $classname . '.php'); 	
	});
	
	$db = new Database();
	require_once(__DIR__ .'/Twig/Twig-1.22.3/lib/Twig/Autoloader.php');
	Twig_Autoloader::register();
?>