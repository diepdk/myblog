<?php 

class Database extends PDO{
	private $host = DB_HOST;
	private $user = DB_USER;
	private $pass = DB_PWD;
	private $database = DB_NAME;
	private $connection = null;
	private $stmt;
	private $error;

	public function __construct(){
		$this->connect();
	}
	public function connect(){
		if($this->connection == null){
			try{
				$this->connection = new PDO('mysql:host='.$this->host.';dbname=' .$this->database, $this->user , $this->pass );
				$this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
				// echo "ok";
			}catch(Exception $ex){
				echo "Errr" . $ex->getMessage();
				die();
			}
		}
		return $this->connection;
	}

	public function query($sql){
		$this->stmt =  $this->connection->prepare($sql);
	}

	public function bind($params_arr){
		foreach ($params_arr as $param => $value) {
			# code...
			$this->stmt->bindValue($param,$value);
		}
	}

	public function execute(){
		return $this->stmt->execute();
	}

	//INSERT
	public function insert($tablename,$data){
		// lay key cua mang $data
		$key = array_keys($data);
		// lay cac params su dung ky tu :, noi (:param,:param)..
		$param = ':' . implode(',:', $key);
		$key = implode(',', $key);
		// insert into tablename (id,name) value (:id,:name);
		$sql = 'INSERT INTO $tablename ($key) VALUES ($param)';
		$this->query($sql);
		$data = $this->renameKey($data,':');
		$this->bind($data);
		try{
			$this->execute();
		}catch(Exception $ex){
			return $this->getMessage();
		}
		return true;
		exit();
	}
	// UPDATE  table SET title=:title , name=:name WHERE id=:_id
	public function update($tablename,$data,$where){
		$key = array_keys($data);
		$query = $this->lastSetUpdate($key);
		$key2 = array_keys($where);
		$wheres = $this->lastWhere($key2);
		$sql = 'UPDATE $tablename SET $query WHERE $wheres';
		$this->query($sql);
		//bindValue
		$data = $this->renameKey($data,':');
		$where = $this->renameKey($where,":_");
		$this->bind($data);
		$this->bind($where);
		try{
			$this->execute();
		}catch(Exception $ex){
			return $this->getMessage();
		}
		return true;
		exit();
	}

	// DELETE table WHERE id=:_id

	public function delete($tablename,$data){
		$key = array_keys($data);

		$key = $this->lastWhere($data);
		$sql = 'DELETE $tablename WHERE $key';
		$this->query($sql);
		$data = $this->renameKey($data);
		$this->bind($data);
		try{
			$this->execute();
		}catch(Exception $ex){
			return $this->getMessage();
		}
		return true;
		exit();
	}
	//creat (title=:title , name=:name)
	public function lastSetUpdate($param){
		$query = [];
		for($i=0 ; $i<count($param) ; $i++){
			$query[] = $param[$i].'=:'.$param[$i];
		}
		return implode(',', $query);
	}

	//creat (id=:_id)
	public function lastWhere($param){
		$query = [];
		for($i=0 ; $i<count($param) ; $i++){
			$query[] = $param[$i].'=:_'.$param[$i];
		}
		return implode(',', $query);
	}
	public function renameKey($arr,$prefix){
		foreach ($arr as $key => $value) {
				// # code...
				// $key = $prefix . $key;
			$arr[$prefix . $key] = $value;
			unset($arr[$key]);
		}
		return $arr;
	}
	//doc data
	public function findAll(){
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	// doc 1 dong

	public function findOne(){
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_ASSOC);
	}

	//dem ban ghi

	public function rowCount(){
		return $this->stmt->rowCount();
	}
}
?>